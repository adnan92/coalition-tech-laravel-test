<html>
    <head>
        <title>Coalition Laravel Test</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
    </head>

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <body>

        <div class="container-fluid" style="margin-top: 50px">
            {!! Form::open(['url'=>URL::route('products.store')]) !!}
            <div class="container"   style="width: 600px;">
                <div class="panel panel-primary">
                    <div class="panel-heading">Information of Product</div>
                    <div class="panel-body">
                        <div class="bs-example">
                        </div>
                        <div class="form-group">
                            {!! Form::label('name' , "Product Name") !!} <span class="text text-danger">*</span>
                            <div class="row">
                                <div class="col-md-12">
                                    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Product Name', 'required' => 'true']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('qty' , "Quantity") !!} <span class="text text-danger">*</span>
                            <div class="row">
                                <div class="col-md-12">
                                    {!! Form::text('quantity', null, ['class' => 'form-control', 'placeholder' => 'Quantity', 'required' => 'true']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('price' , "Price") !!} <span class="text text-danger">*</span>
                            <div class="row">
                                <div class="col-md-12">
                                    {!! Form::text('price', null, ['class' => 'form-control', 'placeholder' => 'Price', 'required' => 'true']) !!}
                                </div>
                            </div>
                        </div>
                        {!! isset($data) ? Form::hidden('address[id]', $data->people->addresses->first()->id) : '' !!}
                        <div class="box-footer pull-right">
                            {!! Form::submit('Save',['class'=>'btn btn-primary']) !!}
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
            <div class="container">
                <table id="dataTableSelect" class="table table-bordered table-striped display">
                    <thead>
                        <tr>
                            <th>Product Name</th>
                            <th>Qty</th>
                            <th>Created</th>
                            <th>Total</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(!empty($products))
                        @foreach($products as $key => $record)
                        <tr>
                            <td>{{$record->name}}</td>
                            <td>{{$record->qty}}</td>
                            <td>{{$record->price}}</td>
                            <td>{{$record->created_at}}</td>
                            <td class="text-center">
                                <a class="btn btn-default btn-sm" href="{{ URL::route('products.index',array('id'=>$record->id)) }}"><span class="fa fa-edit" aria-hidden="true"></span></a>
                                <a class="btn btn-default btn-sm ajax-del targetDel-{{ $record->people->id }}"
                                   data-toggle="modal" data-target="#delForm"
                                   data-url="{{ route('products.index', $record->id) }}"
                                   data-text="{{$record->name}}"
                                   data-id="{{ $record->id }}"
                                   href="#" >
                                    <span class="fa fa-trash" aria-hidden="true">
                                    </span>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Product Name</th>
                            <th>Qty</th>
                            <th>Created</th>
                            <th>Total</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                </table>
            </div>

        </div>
    </body>
</html>
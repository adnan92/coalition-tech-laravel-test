<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

class ProductController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $doc = new \DOMDocument();
        $doc->load('employees.xml');
        $employees = $doc->getElementsByTagName("employee");
        $doc->load('employees.xml');
        print_r($xml);
        return view('products.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $data = $request->all();
//        dd($data);

        $file = storage_path('products.xml');

        $xml = simplexml_load_file($file);
//dd($xml);
        $products = $xml->products;

        $product = $products->addChild('product');
        $product->addChild('name', 'a gallery');
        $product->addChild('qty', 'path/to/gallery');
        $product->addChild('price', 'mythumb.jpg');
        $product->addChild('created_at', 'mythumb.jpg');

        $xml->asXML($file);

//        $doc = new \DOMDocument();
//        $doc->load(storage_path('products.xml'));
//
//        $r = $doc->getElementsByTagName("products");
////        $doc->appendChild($r);
//
//        $b = $doc->createElement("product");
//
//        $name = $doc->createElement("name");
//        $name->appendChild(
//                $doc->createTextNode($data['name'])
//        );
//        $b->appendChild($name);
//
//        $qty = $doc->createElement("quantity");
//        $qty->appendChild(
//                $doc->createTextNode($data['quantity'])
//        );
//        $b->appendChild($qty);
//
//        $price = $doc->createElement("price");
//        $price->appendChild(
//                $doc->createTextNode($data['price'])
//        );
//        $b->appendChild($price);
//
//        $r->appendChild($b);
//
//        $doc->save(storage_path("/products.xml"));


        return redirect()->route('products.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

}
